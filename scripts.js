﻿(function(){
	var link = document.createElement('link');
	link.href = '/projects/HappyBooth/styles.css';
	link.setAttribute('rel', 'stylesheet');
	document.querySelector('head').appendChild(link);
})();


function ValidPhone(form) {
    var re = /^\d[\d\(\)\ -]{4,14}\d$/;
    var myPhone = document.getElementById('phone_number').value;
    var valid = re.test(myPhone);
    if (valid) {
    	var output = 'Отправлено!';
    	document.getElementById('message').style.display = "block";
    	document.getElementById('message').innerHTML = output;
    	document.getElementById('message').style.color = "white";
    	document.getElementById('message').style.fontSize = "1em";
    	document.getElementById('message').style.fontWeight = '600';	
    	return true;
    }
    else 
    {
    	var output = 'Номер телефона введен неправильно!';
    document.getElementById('message').style.display = "block";
    document.getElementById('message').innerHTML = output;
    document.getElementById('message').style.color = "red";
    document.getElementById('message').style.fontSize = "1em";
    document.getElementById('message').style.fontWeight = '600';
    document.getElementById('phone_number').value = "";
    return false;
	}
}  
function focus(){
		setTimeout(function() {
		document.getElementById("phone_number").focus();
	}, 2000);
}
	var t;
	function scroll_up() {
    	var top = Math.max(document.body.scrollTop,document.documentElement.scrollTop);
   		if(top > 0) {
    		window.scrollBy(0,((top+500)/-35));
    		t = setTimeout('scroll_up()',40);
   		}
   		return false;
	}
	var order = document.getElementsByClassName('order');
	var order_small = document.getElementsByClassName('order-small');
	for(var t = 0;t<order.length;t++){
		order[t].addEventListener("click", scroll_up);
		order[t].addEventListener("click", focus);
	}
	for(var t = 0;t<order_small.length;t++){
		order_small[t].addEventListener("click", scroll_up);
		order_small[t].addEventListener("click", focus);
	}
	function correctSize() {
		if(document.getElementById('galery_image').style.padding != "0px"){
			document.getElementById('galery_image').style.padding = 0 + "px";
		}
		var elem = document.getElementById('galery_image');

		var clientWidth = document.documentElement.clientWidth;
		var clientHeight = document.documentElement.clientHeight;
		var imageWidth = elem.offsetWidth;
		var imageHeight = elem.offsetHeight;

		var diffWidth = (clientWidth - imageWidth)/2;
		var diffHeight = (clientHeight - imageHeight)/2;
		if(diffWidth > 0){
			exit.style.left = imageWidth + diffWidth - 54 + "px";
		}else{
			exit.style.left = "calc(100% - 54px)";
			diffWidth = 0;
		}
		if(diffHeight > 0){
			exit.style.bottom = imageHeight + diffHeight - 54 + "px";
		}else{
			exit.style.bottom = "100%";
			diffHeight = 0;
		}

		elem.style.padding = diffHeight + "px " + diffWidth + "px";
		elem.style.width = imageWidth;
		document.getElementById('galery_image').style.visibility ="visible";
	}
		var elem = document.getElementsByClassName('img_div');
		var e = document.getElementsByClassName('zoom');
		var exit = document.getElementById('exit');
		var images_arr = [];

		function loadPHP(){
			    var req = new XMLHttpRequest(); 
			    req.open('GET', 'galery.php', true); 
			    req.send();
			}
			function galeryRequest(){
			    var req = new XMLHttpRequest();  
			    req.onreadystatechange = function() { 
			        if (req.readyState == XMLHttpRequest.DONE) {
			            if(req.status === 200) {
			               window.images_arr = JSON.parse(req.responseText);
			               sortImages();
			            }
			        }
			    }
			    req.open('GET', 'images.json', true); 
			    req.send();
			    window.images_arr = ['images/galery/image0.jpg', 'images/galery/image1.jpg', 'images/galery/image2.jpg', 'images/galery/image3.jpg', 'images/galery/image4.jpg', 'images/galery/image5.jpg', 'images/galery/image6.jpg', 'images/galery/image7.jpg', 'images/galery/image9.jpg'];
			}
			
		loadPHP();
		galeryRequest();
		
		var	current_slide;
		var galery_arrow_forward = document.getElementById('galery_arrow_forward');
		var galery_arrow_back = document.getElementById('galery_arrow_back');
			function slide_forward(current_slide) {
				if(current_slide < images_arr.length -1 ){
					document.getElementById('galery_image').src = images_arr[current_slide + 1];
					document.getElementById('galery_image').onload = function() {correctSize();}
					current_slide = current_slide + 1;
				}
				else{
					document.getElementById('galery_image').src = images_arr[0];
					document.getElementById('galery_image').onload = function() {correctSize();}
					current_slide = 0;
				}
				return current_slide;
			}
			function slide_back(current_slide) {
				if(current_slide > 0){
					document.getElementById('galery_image').src = images_arr[current_slide - 1];
					document.getElementById('galery_image').onload = function() {correctSize();}
				current_slide = current_slide - 1;
			}
				else{
					document.getElementById('galery_image').src = images_arr[images_arr.length - 1];
					document.getElementById('galery_image').onload = function() {correctSize();}
				current_slide = images_arr.length - 1;
			}
			return current_slide;
		}
			e[0].addEventListener("click", function(){
				document.querySelector('.fade_wrap').style.opacity = "0.1";
				document.getElementsByTagName('html')[0].style.overflow = "hidden";
				document.getElementById('galery_image').src = "images/galery/image0.jpg";
				exit.style.display = "block";
				document.querySelector('.fade_container').style.display = "block";
				galery_arrow_forward.style.display = "block";
				galery_arrow_back.style.display = "block";
				var index = document.getElementById('galery_image').src.indexOf("images/galery/image");
				var str = document.getElementById('galery_image').src.substr(index+19);
				current_slide = parseInt(str);
				correctSize();

			});
			e[1].addEventListener("click", function(){
				document.querySelector('.fade_wrap').style.opacity = "0.1";
				document.getElementsByTagName('html')[0].style.overflow = "hidden";
				document.getElementById('galery_image').src = "images/galery/image1.jpg";
				exit.style.display = "block";
				document.querySelector('.fade_container').style.display = "block";
				galery_arrow_forward.style.display = "block";
				galery_arrow_back.style.display = "block";
				var index = document.getElementById('galery_image').src.indexOf("images/galery/image");
				var str = document.getElementById('galery_image').src.substr(index+19);
				current_slide = parseInt(str);
				correctSize();

			});
			e[2].addEventListener("click", function(){
				document.querySelector('.fade_wrap').style.opacity = "0.1";
				document.getElementsByTagName('html')[0].style.overflow = "hidden";
				document.getElementById('galery_image').src = "images/galery/image2.jpg";
				exit.style.display = "block";
				document.querySelector('.fade_container').style.display = "block";
				galery_arrow_forward.style.display = "block";
				galery_arrow_back.style.display = "block";
				var index = document.getElementById('galery_image').src.indexOf("images/galery/image");
				var str = document.getElementById('galery_image').src.substr(index+19);
				current_slide = parseInt(str);
				correctSize();

			});
			e[3].addEventListener("click", function(){
				document.querySelector('.fade_wrap').style.opacity = "0.1";
				document.getElementsByTagName('html')[0].style.overflow = "hidden";
				document.getElementById('galery_image').src = "images/galery/image3.jpg";
				exit.style.display = "block";
				document.querySelector('.fade_container').style.display = "block";
				galery_arrow_forward.style.display = "block";
				galery_arrow_back.style.display = "block";
				var index = document.getElementById('galery_image').src.indexOf("images/galery/image");
				var str = document.getElementById('galery_image').src.substr(index+19);
				current_slide = parseInt(str);
				correctSize();
			});
		exit.addEventListener("click", function(){
			document.querySelector('.fade_wrap').style.opacity = "1";
			document.getElementsByTagName('html')[0].style.overflow = "visible";
			document.getElementById('galery_image').src = "";
			exit.style.display = "none";
			document.querySelector('.fade_container').style.display = "none";
			galery_arrow_forward.style.display = "none";
			galery_arrow_back.style.display = "none";

		});
			galery_arrow_forward.onclick = function() {
				document.getElementById('galery_image').style.padding = 0 + "px";
				document.getElementById('galery_image').style.visibility ="hidden";
				current_slide = slide_forward(current_slide);
			}
			galery_arrow_back.onclick = function() {
				document.getElementById('galery_image').style.padding = 0 + "px";
				document.getElementById('galery_image').style.visibility ="hidden";
				current_slide = slide_back(current_slide);
			}
			elem[0].addEventListener("mouseover", function() {
				e[0].style.visibility = "visible";
				e[0].style.opacity = "1";
			});
			elem[0].addEventListener("mouseout", function() {
				e[0].style.visibility = "hidden";
				e[0].style.opacity = "0";
			});

			elem[1].addEventListener("mouseover", function() {
				e[1].style.visibility = "visible";
				e[1].style.opacity = "1";
			});
			elem[1].addEventListener("mouseout", function() {
				e[1].style.visibility = "hidden";
				e[1].style.opacity = "0";
			});

			elem[2].addEventListener("mouseover", function() {
				e[2].style.visibility = "visible";
				e[2].style.opacity = "1";
			});
			elem[2].addEventListener("mouseout", function() {
				e[2].style.visibility = "hidden";
				e[2].style.opacity = "0";
			});

			elem[3].addEventListener("mouseover", function() {
				e[3].style.visibility = "visible";
				e[3].style.opacity = "1";
			});
			elem[3].addEventListener("mouseout", function() {
				e[3].style.visibility = "hidden";
				e[3].style.opacity = "0";
			});

		var comments = ["Отработали на отлично! Заказали на свадьбу, доставили вовремя и все работало как часы, спасибо happy-booth!", "Cтавили на открытие магазина,такого успеха и не предполагали! Очередь была нескончаемой. Спасибо за работу", "Интересная вещь,заказывали впервые и остались довольны. Советую, для развлечения самое то!", "Happy-booth ,Вы супер!!!!!!!", "Спасибо за доставленные эмоции, в памяти останется надолго, правда гости забыли про еду и стояли в очереди к будке!)"];
		var arrow1 = document.getElementById('ar1');
		var arrow2 = document.getElementById('ar2');
		var par = document.getElementById('com0');
		var num = parseInt(par.id.substring(3)); 
		arrow1.onclick = function() {
			par.style.textIndent = "-7%";
			if(num > 0)
			var t = num -1;
			else
			var t = comments.length -1;
			par.innerHTML = comments[t];
			num = t;
		}
		arrow2.onclick = function() {
			par.style.textIndent = "-7%";
			if(num < comments.length -1)
			var t = num + 1;
			else
			var t = 0;
			par.innerHTML = comments[t];
			num = t;
		}
